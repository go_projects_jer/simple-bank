package utils

// errors
var (
	ErrTimeout = "timeout error ❌"
	ErrDBMigrations = "failed to run migrations"
	ErrApiInitial = "Api initial error"
	ErrIncorrectPassword = "incorrect password"
)

// warnings
var (
	WarnDBNotConnected = "database is not connected"
)